//
//  main.cpp
//  LightfieldViewer
//
//  Created by Alex Trytko on 6/4/13.
//  Copyright (c) 2013 Alex Trytko. All rights reserved.
//

//http://41j.com/blog/2011/10/simple-libtiff-example/
//http://research.cs.wisc.edu/graphics/Courses/638-f1999/libtiff_tutorial.htm
//http://stackoverflow.com/questions/686353/c-random-float

#include "tiffio.h"
#include "LensletGrid.h"
#include "Structure3D.h"
#include "pbrtFunctions.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <math.h>
#include <time.h>
#include <sstream>

using namespace std;

// Based on GET functions at http://www.libtiff.org/man/TIFFReadRGBAImage.3t.html
// Note that abgr must be initialized to 0
void SetR(uint32* abgr, int val) {
    unsigned char valChar = (unsigned char)val;
    *abgr = *abgr | (valChar);
}

void SetG(uint32* abgr, int val) {
    unsigned char valChar = (unsigned char)val;
    *abgr = *abgr | (valChar << 8);
}

void SetB(uint32* abgr, int val) {
    unsigned char valChar = (unsigned char)val;
    *abgr = *abgr | (valChar << 16);
}

void SetA(uint32* abgr, int val) {
    unsigned char valChar = (unsigned char)val;
    *abgr = *abgr | (valChar << 24);
}

static void printLensletGrid(LensletGrid lensletGrid) {
    fprintf(stderr,"Lenslet Array Structure:\n");
    fprintf(stderr,"\tlensesX: %d\n",lensletGrid.lensesX);
    fprintf(stderr,"\tlensesY: %d\n",lensletGrid.lensesY);
    fprintf(stderr,"\tlensletWidth: %f\n",lensletGrid.lensletWidth);
    fprintf(stderr,"\tfocalLength: %f\n",lensletGrid.focalLength);
    fprintf(stderr,"\tfNumber: %f\n",lensletGrid.fNumber);
    fprintf(stderr,"\tfilmdiag: %f\n",lensletGrid.filmdiag);
    fprintf(stderr,"\txRes: %d\n",lensletGrid.xRes);
    fprintf(stderr,"\tyRes: %d\n",lensletGrid.yRes);
    fprintf(stderr,"\tlensletDistance: %f\n",lensletGrid.lensletDistance);
    fprintf(stderr,"\tapertureDiameter: %f\n",lensletGrid.apertureDiameter);
    fprintf(stderr,"\n");
}

LensletGrid buildLensletGrid(const char* lensletSpecFilename) {
    fprintf(stderr,"Building lensletGrid with spec %s\n",lensletSpecFilename);
    LensletGrid lensletGrid;
    ifstream mylensletspecfile (lensletSpecFilename);
    string line;
    if (mylensletspecfile.is_open()) {
        while (mylensletspecfile.good()) {
            getline (mylensletspecfile,line);
            char *linechars = new char [line.length()+1];
            strcpy(linechars,line.c_str());
            if (strcmp(linechars,"") != 0) {
                char *firsttoken = strtok(linechars,"\t");
                char *secondtoken = strtok(NULL,"\t");
                float value = atof(secondtoken);
                if (strcmp(firsttoken,"lensesX") == 0) {
                    lensletGrid.lensesX = int(value);
                } else if (strcmp(firsttoken,"lensesY") == 0) {
                    lensletGrid.lensesY = int(value);
                } else if (strcmp(firsttoken,"fNumber") == 0) {
                    lensletGrid.fNumber = value;
                } else if (strcmp(firsttoken,"filmdiag") == 0) {
                    lensletGrid.filmdiag = value;
                } else if (strcmp(firsttoken,"xRes") == 0) {
                    lensletGrid.xRes = int(value);
                } else if (strcmp(firsttoken,"yRes") == 0) {
                    lensletGrid.yRes = int(value);
                } else if (strcmp(firsttoken,"lensletDistance") == 0) {
                    lensletGrid.lensletDistance = value;
                } else if (strcmp(firsttoken,"apertureDiameter") == 0) {
                    lensletGrid.apertureDiameter = value;
                } else if (strcmp(firsttoken,"backMainLensAperture") == 0) {
                    lensletGrid.backMainLensAperture = value;
                }
            }
        }
        mylensletspecfile.close();
    }
    
    
    
    float filmx = sqrt(pow(lensletGrid.filmdiag,2)/(1+float(lensletGrid.yRes)/lensletGrid.xRes));
    lensletGrid.lensletWidth = filmx / lensletGrid.lensesX;
    lensletGrid.focalLength = lensletGrid.lensletWidth * lensletGrid.fNumber;
    
    
    
    return lensletGrid;
}

void makeMiddleSubapertureImage(uint32* lfi, LensletGrid lensletGrid) {
    int w = 150;
    int h = 150;
    int sampleperpixel = 4;
    TIFF *out = TIFFOpen("/Users/alextrytko/Desktop/Coding/LightfieldViewer/LightfieldViewer/subap.tiff","w");
    TIFFSetField(out,TIFFTAG_IMAGEWIDTH,w);
    TIFFSetField(out,TIFFTAG_IMAGELENGTH,h);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL,sampleperpixel);
    TIFFSetField(out,TIFFTAG_BITSPERSAMPLE,8);
    TIFFSetField(out,TIFFTAG_ORIENTATION,ORIENTATION_TOPLEFT);
    TIFFSetField(out,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
    TIFFSetField(out,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_RGB);
    tsize_t linebytes = sampleperpixel * w;
    unsigned char *buf = NULL;
    if (TIFFScanlineSize(out) < linebytes) {
        buf = (unsigned char *)_TIFFmalloc(linebytes);
    } else {
        buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
    }
    
    TIFFSetField(out,TIFFTAG_ROWSPERSTRIP,TIFFDefaultStripSize(out,w*sampleperpixel));
    
    float lensletPixelWidth = 2400.0 / 300.0;
    cout << lensletPixelWidth << endl;
    
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            int totalX = floor(lensletPixelWidth * (x + 0.5));
            int totalY = floor(2400 - lensletPixelWidth * (y + 0.5));
            cout << "(" << totalX << "," << totalY << ")" << endl;
            int index = totalY*300 + totalX;
            uint32 pixel = lfi[index];
            memcpy(buf+x*4,&pixel,sizeof(uint32));
        }
        if (TIFFWriteScanline(out,buf,y,0) < 0) {
            break;
        }
    }
    TIFFClose(out);
    if (buf) {
        _TIFFfree(buf);
    }
    cout << "Wrote subap file" << endl;

}

unsigned char* getOneSubaperture(string subapertureFilename, int subap_x, int subap_y, LensletGrid lensletGrid,
                       string outname) {
    TIFF* tif = TIFFOpen(subapertureFilename.c_str(),"r");
    uint32* fullSubaperture;
    uint32 w, h;
    if (tif) {
        size_t npixels;
        
        TIFFGetField(tif,TIFFTAG_IMAGEWIDTH,&w);
        TIFFGetField(tif,TIFFTAG_IMAGELENGTH,&h);
        npixels = w * h;
        cout << "Width: " << w << "; Height: " << h << endl;
        
        fullSubaperture = (uint32*) _TIFFmalloc(npixels * sizeof (uint32));
        if (fullSubaperture != NULL) {
            if (TIFFReadRGBAImage(tif, w, h, fullSubaperture, 0)) {
                cout << "Read in tiff file" << endl;
            }
        }
        TIFFClose(tif);
    }
    int one_w = lensletGrid.lensesX;
    int one_h = lensletGrid.lensesY;
    int sampleperpixel = 4;
    TIFF *out = TIFFOpen(outname.c_str(),"w");
    TIFFSetField(out,TIFFTAG_IMAGEWIDTH,one_w);
    TIFFSetField(out,TIFFTAG_IMAGELENGTH,one_h);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL,sampleperpixel);
    TIFFSetField(out,TIFFTAG_BITSPERSAMPLE,8);
    TIFFSetField(out,TIFFTAG_ORIENTATION,ORIENTATION_TOPLEFT);
    TIFFSetField(out,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
    TIFFSetField(out,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_RGB);
    
    tsize_t linebytes = sampleperpixel * one_w;
    unsigned char *buf = NULL;
    if (TIFFScanlineSize(out) < linebytes) {
        buf = (unsigned char *)_TIFFmalloc(linebytes);
    } else {
        buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
    }
    
    unsigned char* all = (unsigned char *)_TIFFmalloc(one_w * one_h * sampleperpixel);
    
    TIFFSetField(out,TIFFTAG_ROWSPERSTRIP,TIFFDefaultStripSize(out,one_w*sampleperpixel));
    
    for (int y = 0; y < one_h; y++) {
        for (int x = 0; x < one_w; x++) {
            int totalX = subap_x*lensletGrid.lensesX + x;
            int totalY = h - 1 - (subap_y*lensletGrid.lensesY + y);
            int index = totalY*w + totalX;
            uint32 pixel = fullSubaperture[index];
            memcpy(buf+x*4,&pixel,sizeof(uint32));
            memcpy(all+x*4+y*4*one_w,&pixel,sizeof(uint32));
        }
        if (TIFFWriteScanline(out,buf,y,0) < 0) {
            break;
        }
    }
    TIFFClose(out);
    
    cout << "Wrote " << outname << endl;
    return all;
}

void combineSubapertures(unsigned char** subapertures, int numX, int numY, int w, int h, float alpha, string outName, int lensesX, int lensesY) {
    cout << "Combining with alpha" << alpha << endl;
    unsigned int* newImage = (unsigned int*)calloc(sizeof(unsigned int),w*h*4);// unsigned int[w*h];
    for (int i = 0; i < 7; i++) {
        for (int j = 1; j < 8; j++) {
            // u is where the center is, [-1,1]
            float u = (-4.0 + i)/(4.0) + (1/8.0);
            float v = (-4.0 + j)/(4.0) + (1/8.0);
            float shiftX = u * (1 - 1.0/alpha);
            float shiftY = v * (1 - 1.0/alpha);
            for (int pi = 0; pi < w*4; pi++) {
                for (int pj = 0; pj < h; pj++) {
                    int lensletX = floor(pi/4*(float)lensesX/w);
                    int lensletY = floor(pj*(float)lensesY/h);
                    
                    
                    int xToSample = (int)floor((lensletX - shiftX*(float)lensesX));
                    int yToSample = (int)floor((lensletY - shiftY*(float)lensesY));
                    if (xToSample < lensesX && xToSample >= 0 && yToSample < lensesY && yToSample >= 0) {
                        newImage[pi+pj*w*4] += subapertures[i+j*numX][xToSample*4+pi%4+yToSample*lensesX*4];
                    } else {
                        newImage[pi+pj*w*4] += 0;
                    }
                }
            }
        }
    }
    for (int pi = 0; pi < w*4; pi++) {
        for (int pj = 0; pj < h; pj++) {
            newImage[pi+pj*w*4] /= (8*8);
        }
    }
    int sampleperpixel = 4;
    TIFF *out = TIFFOpen(outName.c_str(),"w");
    TIFFSetField(out,TIFFTAG_IMAGEWIDTH,w);
    TIFFSetField(out,TIFFTAG_IMAGELENGTH,h);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL,sampleperpixel);
    TIFFSetField(out,TIFFTAG_BITSPERSAMPLE,8);
    TIFFSetField(out,TIFFTAG_ORIENTATION,ORIENTATION_TOPLEFT);
    TIFFSetField(out,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
    TIFFSetField(out,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_RGB);
    
    tsize_t linebytes = sampleperpixel * w;
    unsigned char *buf = NULL;
    if (TIFFScanlineSize(out) < linebytes) {
        buf = (unsigned char *)_TIFFmalloc(linebytes);
    } else {
        buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
    }
    
    TIFFSetField(out,TIFFTAG_ROWSPERSTRIP,TIFFDefaultStripSize(out,w*sampleperpixel));
    
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            int A = 255;
            
            uint32 synthesizedPixel = 0;
            SetR(&synthesizedPixel,newImage[x*4+y*w*4]);
            SetG(&synthesizedPixel,newImage[x*4+y*w*4+1]);
            SetB(&synthesizedPixel,newImage[x*4+y*w*4+2]);
            SetA(&synthesizedPixel,A);
            
            memcpy(buf+x*4,&synthesizedPixel,sizeof(uint32));
        }
        if (TIFFWriteScanline(out,buf,y,0) < 0) {
            break;
        }
    }
    TIFFClose(out);
    if (buf) {
        _TIFFfree(buf);
    }

}

void makeSubapertureImage(uint32* lfi, LensletGrid lensletGrid) {
    int lensesX = lensletGrid.lensesX;
    int lensesY = lensletGrid.lensesY;
    float lensletPixelWidth = (float)lensletGrid.xRes / lensletGrid.lensesX;
    int w = lensletGrid.xRes;
    int h = lensletGrid.yRes;
    
    int sampleperpixel = 4;
    TIFF *out = TIFFOpen("/Users/alextrytko/Desktop/Coding/LightfieldViewer/LightfieldViewer/subaperture.tiff","w");
    TIFFSetField(out,TIFFTAG_IMAGEWIDTH,w);
    TIFFSetField(out,TIFFTAG_IMAGELENGTH,h);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL,sampleperpixel);
    TIFFSetField(out,TIFFTAG_BITSPERSAMPLE,8);
    TIFFSetField(out,TIFFTAG_ORIENTATION,ORIENTATION_TOPLEFT);
    TIFFSetField(out,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
    TIFFSetField(out,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_RGB);
    
    tsize_t linebytes = sampleperpixel * w;
    unsigned char *buf = NULL;
    if (TIFFScanlineSize(out) < linebytes) {
        buf = (unsigned char *)_TIFFmalloc(linebytes);
    } else {
        buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
    }
    
    TIFFSetField(out,TIFFTAG_ROWSPERSTRIP,TIFFDefaultStripSize(out,w*sampleperpixel));
    
    cout << lensletPixelWidth << endl;
    
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            int squareNumX = floor(x/lensesX);
            int squareNumY = floor(y/lensesY);
            // Above will be the num of the lens-area to extract
            
            // Within each of these squares, there should be lensesX by lensesY pixels to sample
            int lensX = x - squareNumX*lensesX;
            int lensY = lensesY - 1 - (y - squareNumY*lensesY);
            
            
            int totalX = lensX*lensletPixelWidth + squareNumX;
            int totalY = lensY*lensletPixelWidth + squareNumY;
            int index = totalY*w + totalX;
            uint32 pixel = lfi[index];
            memcpy(buf+x*4,&pixel,sizeof(uint32));
        }
        if (TIFFWriteScanline(out,buf,y,0) < 0) {
            break;
        }
    }
    TIFFClose(out);
    if (buf) {
        _TIFFfree(buf);
    }
    cout << "Wrote subap file" << endl;
}

Ray3D constructRayFromMainLensToVirtualFilm(int px, int py, LensletGrid lensletGrid, float delta,
                                                  int numPixelsX, int numPixelsY, float dxLens, float dyLens) {
    float filmx = sqrt(pow(lensletGrid.filmdiag,2)/(1+float(lensletGrid.yRes)/lensletGrid.xRes));
    float filmy = filmx * float(lensletGrid.yRes)/lensletGrid.xRes;
    
    float film_xfrac = float(px) / numPixelsX;
    float film_yfrac = float(py) / numPixelsY;
    float startx = (-filmx/2.0 + film_xfrac*filmx);
    float starty = (-filmy/2.0 + film_yfrac*filmy);
    float startz = -lensletGrid.lensletDistance + delta; // z=0 is back of main lens array
    Vector3D startPos = Vector3D(startx,starty,startz);
    
    
    float lensx = dxLens * lensletGrid.backMainLensAperture/2.0;
    float lensy = dyLens * lensletGrid.backMainLensAperture/2.0;
    float lensz = 0.0; // by definition above
    Vector3D lensPos = Vector3D(lensx,lensy,lensz);
    
    Vector3D dir = lensPos.dirToPoint(startPos);
    dir.Normalize();
    
    return Ray3D(lensPos,dir);
}

void findWhichLenslet(LensletGrid lensletGrid,int* lensletX,int* lensletY,Vector3D intersect) {
    float lensletLengthX = lensletGrid.lensesX * lensletGrid.lensletWidth;
    float lensletLengthY = lensletGrid.lensesY * lensletGrid.lensletWidth;
    float x_frac = (intersect.x - (-lensletLengthX/2.0)) / lensletLengthX;
    float y_frac = (intersect.y - (-lensletLengthY/2.0)) / lensletLengthY;
    *lensletX = floor(x_frac * lensletGrid.lensesX);
    *lensletY = floor(y_frac * lensletGrid.lensesY);
}

Vector3D getLensletCenter(int lensletX, int lensletY, LensletGrid lensletGrid) {
    float lensletLengthX = lensletGrid.lensesX * lensletGrid.lensletWidth;
    float lensletLengthY = lensletGrid.lensesY * lensletGrid.lensletWidth;
    float xPos = (-lensletLengthX/2.0) + lensletGrid.lensletWidth * (lensletX + 0.5);
    float yPos = (-lensletLengthY/2.0) + lensletGrid.lensletWidth * (lensletY + 0.5);
    float zPos = -lensletGrid.lensletDistance;
    return Vector3D(xPos,yPos,zPos);
}

void findWhichLFIPixel(float LFIFilmLengthX,float LFIFilmLengthY, Vector3D positionOnLFIFilm,
                       int LFIXRes, int LFIYRes, int* lfiPixelX, int* lfiPixelY) {
    float x_frac = (positionOnLFIFilm.x - (-LFIFilmLengthX/2.0)) / LFIFilmLengthX;
    float y_frac = (positionOnLFIFilm.y - (-LFIFilmLengthY/2.0)) / LFIFilmLengthY;
    *lfiPixelX = floor(x_frac * LFIXRes);
    *lfiPixelY = floor(y_frac * LFIYRes);
}

bool findLFIPixel(Ray3D ray, LensletGrid lensletGrid, int* lfiPixelX, int* lfiPixelY, float* weight) {
    Vector3D lensletGridIntersect = ray.getIntersectWithZPlane(-lensletGrid.lensletDistance);
    float LFIFilmLengthX = sqrt(pow(lensletGrid.filmdiag,2)/(1+float(lensletGrid.yRes)/lensletGrid.xRes));
    float LFIFilmLengthY = LFIFilmLengthX * float(lensletGrid.yRes)/lensletGrid.xRes;
    int LFIXRes = lensletGrid.xRes;
    int LFIYRes = lensletGrid.yRes;
    float lensletLengthX = lensletGrid.lensesX * lensletGrid.lensletWidth;
    float lensletLengthY = lensletGrid.lensesY * lensletGrid.lensletWidth;
    if ((lensletGridIntersect.x <= lensletLengthX/2.0) && (lensletGridIntersect.x >= -lensletLengthX/2.0)
        && (lensletGridIntersect.y <= lensletLengthY/2.0) && (lensletGridIntersect.y >= -lensletLengthY/2.0)) {
        int lensletX;
        int lensletY;
        findWhichLenslet(lensletGrid,&lensletX,&lensletY,lensletGridIntersect);
        // Since film was focalLength from grid, this ray will go to where the ray through center of
        // lenslet would have gone.
        Vector3D lensletCenter = getLensletCenter(lensletX,lensletY,lensletGrid);
        Vector3D equivalentDirection = ray.origin.dirToPoint(lensletCenter);
        
        Ray3D equivalentRay = Ray3D(ray.origin,equivalentDirection);
        
        
        float lensletDistance = lensletGrid.lensletDistance;
        float focalLength = lensletGrid.focalLength;
        Vector3D positionOnLFIFilm = equivalentRay.getIntersectWithZPlane(-lensletDistance - focalLength);
        positionOnLFIFilm = equivalentRay.getIntersectWithZPlane(-lensletDistance - focalLength);
        /*Vector3D lensletToRealFilmDir = lensletGridIntersect.dirToPoint(positionOnLFIFilm);
        Vector3D toDot = Vector3D(0,0,-1);
        lensletToRealFilmDir.Normalize();
        float costheta = lensletToRealFilmDir.dot(toDot);
        *weight *= pow(costheta,4.0) / float(pow(lensletGrid.focalLength,2)) * ((M_PI) * pow(lensletGrid.lensletWidth/2.0,2));*/
        findWhichLFIPixel(LFIFilmLengthX,LFIFilmLengthY,positionOnLFIFilm,LFIXRes,LFIYRes,lfiPixelX,lfiPixelY);
        
        return true;
    } else {
        return false;
    }
    
}

uint32 getPixel(int x, int y, uint32* image, int imageResX, int imageResY) {
    return image[y*imageResX + x];
}

uint32 synthesizePixel(int x, int y, uint32* lfi, LensletGrid lensletGrid, float delta,
                       int w, int h, int spp, int lfiResX, int lfiResY) {
    int validPixels = 0;
    int totalPixels = 0;
    float totalWeight = 0;
    float totalR = 0;
    float totalG = 0;
    float totalB = 0;
    while (totalPixels < spp) {
        totalPixels++;
        float u1 = (float)rand()/(float)RAND_MAX;
        float u2 = (float)rand()/(float)RAND_MAX;
        float dx;
        float dy;
        ConcentricSampleDisk(u1,u2,&dx,&dy);
        Ray3D mainLensToVirtualFilm = constructRayFromMainLensToVirtualFilm(x,y,lensletGrid,delta,w,h,dx,dy);
        
        float costhetahere = mainLensToVirtualFilm.dir.dot(Vector3D(0,0,-1));
        float weight = pow(costhetahere,4.0) / float(pow(lensletGrid.lensletDistance-delta,2)) * ((M_PI) * pow(lensletGrid.backMainLensAperture/2.0,2));
        
        
        
        // TODO: Next thing to try: also multiply by weight that the ray received in the LFCamera
                
        int lfiPixelX;
        int lfiPixelY;
        if (findLFIPixel(mainLensToVirtualFilm,lensletGrid,&lfiPixelX,&lfiPixelY,&weight)) {
            if ((lfiPixelX < lfiResX) && (lfiPixelY < lfiResY) && (lfiPixelX >= 0) && (lfiPixelY >= 0)) {
                validPixels++;
                totalWeight += weight;
                uint32 pixelSample = getPixel(lfiPixelX,lfiPixelY,lfi,lfiResX,lfiResY);
                totalR += TIFFGetR(pixelSample) * weight;
                totalG += TIFFGetG(pixelSample) * weight;
                totalB += TIFFGetB(pixelSample) * weight;
            }
        }
    }
    float hitRate = (float)validPixels / (float) spp;
    int avgR = totalR / totalWeight;
    int avgG = totalG / totalWeight;
    int avgB = totalB / totalWeight;
    
    int A = 255;
    
    uint32 synthesizedPixel = 0;
    SetR(&synthesizedPixel,avgR);
    SetG(&synthesizedPixel,avgG);
    SetB(&synthesizedPixel,avgB);
    SetA(&synthesizedPixel,A);
    return synthesizedPixel;
}

void synthesizeImage(uint32* lfi, LensletGrid lensletGrid, float delta, int w, int h, int spp, int lfiResX, int lfiResY,
                     string outName) {
    string outPath = "/Users/alextrytko/Desktop/Coding/LightfieldViewer/LightfieldViewer/";
    outPath.append(outName);
    TIFF *out = TIFFOpen(outPath.c_str(),"w");
    TIFFSetField(out,TIFFTAG_IMAGEWIDTH,w);
    TIFFSetField(out,TIFFTAG_IMAGELENGTH,h);
    TIFFSetField(out, TIFFTAG_SAMPLESPERPIXEL,4); //This is for RGBA, NOT for spp
    TIFFSetField(out,TIFFTAG_BITSPERSAMPLE,8);
    TIFFSetField(out,TIFFTAG_ORIENTATION,ORIENTATION_TOPLEFT);
    TIFFSetField(out,TIFFTAG_PLANARCONFIG,PLANARCONFIG_CONTIG);
    TIFFSetField(out,TIFFTAG_PHOTOMETRIC,PHOTOMETRIC_RGB);
    tsize_t linebytes = 4 * w;
    unsigned char *buf = NULL;
    if (TIFFScanlineSize(out) < linebytes) {
        buf = (unsigned char *)_TIFFmalloc(linebytes);
    } else {
        buf = (unsigned char *)_TIFFmalloc(TIFFScanlineSize(out));
    }
    TIFFSetField(out,TIFFTAG_ROWSPERSTRIP,TIFFDefaultStripSize(out,w*4));

    
    
    for (int y = 0; y < h; y++) {
        cout << "Synthesizing line " << y << endl;
        for (int x = 0; x < w; x++) {
            uint32 pixel = synthesizePixel(x,h-y-1,lfi,lensletGrid,delta,w,h,spp,lfiResX,lfiResY);
            memcpy(buf+x*4,&pixel,sizeof(uint32));
        }
        if (TIFFWriteScanline(out,buf,y,0) < 0) {
            break;
        }
    }
    TIFFClose(out);
    if (buf) {
        _TIFFfree(buf);
    }
    cout << "Wrote synthesized image" << endl;
}


int main(int argc, const char * argv[])
{
    LensletGrid lensletGrid = buildLensletGrid(argv[1]);

    // Initialize random generator
    srand((unsigned int)time(NULL));
    
    TIFF* tif = TIFFOpen(argv[2],"r");
    uint32* raster;
    uint32 w, h;
    if (tif) {
        size_t npixels;
        
        TIFFGetField(tif,TIFFTAG_IMAGEWIDTH,&w);
        TIFFGetField(tif,TIFFTAG_IMAGELENGTH,&h);
        npixels = w * h;
        cout << "Width: " << w << "; Height: " << h << endl;
        
        raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32));
        if (raster != NULL) {
            if (TIFFReadRGBAImage(tif, w, h, raster, 0)) {
                cout << "Read in tiff file" << endl;
            }
        }
        TIFFClose(tif);
    
    
        char *image = (char*)raster;
        makeSubapertureImage(raster, lensletGrid);
        
        float lensletPixelWidth = (float)lensletGrid.xRes / (float)lensletGrid.lensesX;
        unsigned char** subapertures = new unsigned char*[8*8];
        
        for (int j = 0; j < 8; j++) {
            for (int i = 0; i < 8; i++) {
                string outname = "/Users/alextrytko/Desktop/Coding/LightfieldViewer/LightfieldViewer/one_subap";
                ostringstream ss;
                ss << "_" << i << "_" << j << ".tiff";
                outname.append(ss.str());
                unsigned char* s = getOneSubaperture("/Users/alextrytko/Desktop/Coding/LightfieldViewer/LightfieldViewer/subaperture.tiff", i, j, lensletGrid, outname);
                subapertures[i+8*j] = s;
            }
        }
        for (float delta = 0.98; delta <= 1.0; delta += 0.005) {
            string outName = "/Users/alextrytko/Desktop/Coding/LightfieldViewer/LightfieldViewer/fcombined_";
            ostringstream ss;
            ss << delta;
            outName.append(ss.str());
            outName.append(".tiff");
            combineSubapertures(subapertures,8,8,1200,1200,delta,outName,lensletGrid.lensesX,lensletGrid.lensesY);

        }
        _TIFFfree(raster);
        cout << "Wrote tiff file" << endl;
    }
}

