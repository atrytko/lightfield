//
//  LensletGrid.h
//  LightfieldViewer
//
//  Created by Alex Trytko on 6/5/13.
//  Copyright (c) 2013 Alex Trytko. All rights reserved.
//

#ifndef __LightfieldViewer__LensletGrid__
#define __LightfieldViewer__LensletGrid__

#include <iostream>
class LensletGrid {
public:
    int lensesX; // Num lenses in x direction
    int lensesY; // Num lenses in y direction. Assuming lensesY == lensesX
    float lensletWidth; // in mm
    float focalLength; // in mm
    float fNumber; //Should simply be focalLength / lensletWidth
    
    // extra information about original PBRT
    float filmdiag;
    int xRes;
    int yRes;
    float lensletDistance;
    float apertureDiameter;
    float backMainLensAperture;
};

#endif /* defined(__LightfieldViewer__LensletGrid__) */
