//
//  pbrtFunctions.h
//  LightfieldViewer
//
//  Created by Alex Trytko on 6/5/13.
//  Copyright (c) 2013 Alex Trytko. All rights reserved.
//

// This file contains functionality copied straight from PBRT

#ifndef __LightfieldViewer__pbrtFunctions__
#define __LightfieldViewer__pbrtFunctions__

#include <iostream>
#include <math.h>

// From http://www.csie.ntu.edu.tw/~cyy/courses/rendering/pbrt-1.04/doxygen/html/mc_8cpp-source.html
void ConcentricSampleDisk(float u1, float u2, float *dx, float *dy);

#endif /* defined(__LightfieldViewer__pbrtFunctions__) */
