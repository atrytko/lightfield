//
//  Structure3D.cpp
//  LightfieldViewer
//
//  Created by Alex Trytko on 6/5/13.
//  Copyright (c) 2013 Alex Trytko. All rights reserved.
//

#include "Structure3D.h"

Vector3D::Vector3D() {
    x = 0;
    y = 0;
    z = 0;
}

Vector3D::Vector3D(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
}

Vector3D::~Vector3D() {
    // Teehee I didn't implement this.
}

Vector3D Vector3D::dirToPoint(Vector3D p2) {
    float deltaX = p2.x - x;
    float deltaY = p2.y - y;
    float deltaZ = p2.z - z;
    
    return Vector3D(deltaX,deltaY,deltaZ);
}

void Vector3D::Normalize() {
    float mag = sqrt(pow(x,2)+pow(y,2)+pow(z,2));
    x = x / mag;
    y = y / mag;
    z = z / mag;
}

float Vector3D::dot(Vector3D v2) {
    return (x*v2.x + y*v2.y + z*v2.z);
}

Ray3D::Ray3D(Vector3D _origin, Vector3D _dir) {
    origin = _origin;
    dir = _dir;
}

Ray3D::~Ray3D() {
    // This either!  What a rebel.
}

Vector3D Ray3D::getIntersectWithZPlane(float zVal) {
    float t = (zVal - origin.z) / dir.z;
    float x = origin.x + t * dir.x;
    float y = origin.y + t * dir.y;
    return Vector3D(x,y,zVal);
}