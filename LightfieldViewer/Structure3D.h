//
//  Structure3D.h
//  LightfieldViewer
//
//  Created by Alex Trytko on 6/5/13.
//  Copyright (c) 2013 Alex Trytko. All rights reserved.
//

#ifndef __LightfieldViewer__Structure3D__
#define __LightfieldViewer__Structure3D__

#include <iostream>
#include <math.h>

class Vector3D {
public:
    float x;
    float y;
    float z;
    
    
    Vector3D(float _x, float _y, float _z);
    Vector3D();
    ~Vector3D();
    float dot(Vector3D v2);
    void Normalize();
    Vector3D dirToPoint(Vector3D p2);
};

class Ray3D {
public:
    Vector3D origin;
    Vector3D dir;
    
    Ray3D(Vector3D _origin, Vector3D _dir);
    ~Ray3D();
    Vector3D getIntersectWithZPlane(float zVal);
};

#endif /* defined(__LightfieldViewer__Structure3D__) */
