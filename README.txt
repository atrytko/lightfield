Files in pbrt-files belong in the following pbrt-v2/src directories:

cameras:
  realistic.cpp
  realistic.h

core:
  api.cpp
  camera.h

film:
  image.cpp
  image.h

renderers:
  samplerrenderer.cpp

The above files are to run pbrt to render the light-field image.  The
camera section of the pbrt file should follow the following format:

Camera "lightfield"
        "string specfile" "dgauss.50mm.dat"
        "string lensletSpec" "lenslets.txt"
        "float lensletDistance" 36.07985
        "float aperture_diameter" 17.1
        "float filmdiag" 70
        "float hither" [0.001 ]
        "float yon" [1e+30 ]
        "float shutteropen" [0 ]
        "float shutterclose" [0 ]
_________________________________________________

Format of lenslet text files:

lensesX 150
lensesY 150
fNumber 2.0 // fNumber of lenslets
filmdiag        70.0
xRes    1200 // resolution of original light-field image
yRes    1200
lensletDistance 36.07985 // distance from back of main lens to lenslet grid
apertureDiameter        17.1 // aperture of the main lens
backMainLensAperture    20.0 // back lens of the main lens system

__________________________________________________

The light-field viewer project is to synthesize images from the rendered 
light-field image.
